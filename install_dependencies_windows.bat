git submodule update --init
git submodule update --recursive
cd External
git clone https://github.com/microsoft/vcpkg.git
cd vcpkg
git pull
git checkout 2bc6cd714
call bootstrap-vcpkg.bat
vcpkg.exe install gtest benchmark --triplet x64-windows --recurse
