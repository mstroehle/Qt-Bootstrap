#pragma once

#include <QObject>
#include <QGuiApplication>
#include <QQuickWindow>
#include <QIcon>
#include <QQmlApplicationEngine>

class App : public QObject
{
    Q_OBJECT
public:
    explicit App(QObject *parent = nullptr);

signals:

public slots:

private:

    QQmlApplicationEngine m_engine;
};
