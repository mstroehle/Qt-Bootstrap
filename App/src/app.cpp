#include "app.h"

App::App(QObject* parent)
    : QObject(parent)
{

    // Use https://www.icoconverter.com/ to convert you logo to ico
    QGuiApplication::setWindowIcon(QIcon(":/Assets/App.ico"));
    QQuickWindow::setTextRenderType(QQuickWindow::TextRenderType::NativeTextRendering);

    m_engine.load(QStringLiteral("qrc:/main.qml"));
}
