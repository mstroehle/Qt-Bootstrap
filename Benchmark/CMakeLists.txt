project(Benchmark)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(benchmark CONFIG REQUIRED)

# Find the Qt libraries for Qt Quick/QML
find_package(
    Qt5
    COMPONENTS
                Quick
                Gui
    REQUIRED)

# add source files
set(src main.cpp)

set(headers
    )

add_executable(${PROJECT_NAME} ${src} ${headers} )


# Use the Qml/Quick modules from Qt 5.
target_link_libraries(${PROJECT_NAME}
    PRIVATE
    Qt5::Gui
    Qt5::Quick
    benchmark::benchmark
    benchmark::benchmark_main)
