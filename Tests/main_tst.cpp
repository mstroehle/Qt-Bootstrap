#include <QGuiApplication>
#include "app.h"
#include <benchmark/benchmark.h>


int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication::setOrganizationName("AppCompany");
    QGuiApplication::setOrganizationDomain("www.example.app");
    QGuiApplication::setApplicationName("App");
    QGuiApplication::setApplicationVersion("1.0.0");

    QGuiApplication guiApp(argc, argv);

    App app;

    return guiApp.exec();
}
