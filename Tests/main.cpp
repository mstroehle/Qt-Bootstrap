#include <QGuiApplication>
#include <QQuickWindow>
#include <QIcon>
#include <QTimer>
#include <gtest/gtest.h>

double subtract_numbers(const double f1, const double f2)
{
    return f1 - f2;
}

TEST(example, subtract)
{
    double res;
    res = subtract_numbers(1.0, 2.0);
    ASSERT_NEAR(res, -1.0, 1.0e-11);
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication::setOrganizationName("AppCompany");
    QGuiApplication::setOrganizationDomain("www.example.app");
    QGuiApplication::setApplicationName("App");
    QGuiApplication::setApplicationVersion("1.0.0");

    QGuiApplication app(argc, argv);

    // Use https://www.icoconverter.com/ to convert you logo to .ico
    QGuiApplication::setWindowIcon(QIcon(":/Assets/App.ico"));
    QQuickWindow::setTextRenderType(QQuickWindow::TextRenderType::NativeTextRendering);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

}
